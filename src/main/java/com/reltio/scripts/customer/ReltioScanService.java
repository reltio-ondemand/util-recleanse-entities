package com.reltio.scripts.customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.reltio.cst.service.ReltioAPIService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ReltioScanService {

    protected String entityType;
    protected String scanFilter;
    protected List<Map<String, Object>> objects = Lists.newArrayList();
    protected List<String> uris = Lists.newArrayList();
    protected String cursor = null;
    protected ObjectMapper mapper = new ObjectMapper();
    protected int fetchIndexFromArray = 0;
    protected boolean scanModeEnabled = false;
    protected int pageSize = 200;
    protected boolean returnUriOnly = false;
    protected String apiUrl;
    protected String selectString = null;
    protected int page = 0;
    protected int maxPages = Integer.MAX_VALUE;

    public void dbscan(String entityType) {
        this.entityType = entityType;
        scanModeEnabled = false;
    }

    public void scan(String scanFilter) {
        this.scanFilter = scanFilter.trim();
        scanModeEnabled = true;
    }

    public boolean hasNext(ReltioAPIService reltioAPI) {
        try {
            if (page >= maxPages) return false;
            
            if (uris != null || page == 0) {
                return true;
            }
            else if (uris == null || uris.isEmpty()) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("An error occurred while fetching list of objects: " + e.getMessage());
        }
        return false;
    }

    public List<String> getNext(ReltioAPIService reltioAPI) {
        try {
            if (hasNext(reltioAPI)) {
                populateObjectsArray(reltioAPI);
                page++;
                return uris;
            }
        } catch (Exception e) {
            System.out.println("An error occurred while fetching list of objects: " + e.getMessage());
        }
        return null;
    }

    public List<Map<String, Object>> getNextList(ReltioAPIService reltioAPI) {
        try {
            if (hasNext(reltioAPI)) {
                fetchIndexFromArray=objects.size();
                return objects;
            }
        } catch (Exception e) {
            System.out.println("An error occurred while fetching list of objects: " + e.getMessage());
        }
        return null;
    }

    public List<String> getNextUri(ReltioAPIService reltioAPI) {

        try {
            if (hasNext(reltioAPI)) {
                fetchIndexFromArray++;
                return uris;
            }
        } catch (Exception e) {
            System.out.println("An error occurred while fetching list of objects: " + e.getMessage());
        }
        return null;

    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setSelectString(String selectString) {
        this.selectString = selectString;
    }


    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public void setReturnUriOnly(boolean returnUriOnly) {
        this.returnUriOnly = returnUriOnly;
    }

    protected void populateObjectArrayIfExhausted(ReltioAPIService reltioAPI) throws Exception {
        if (returnUriOnly) {
            if (uris == null || fetchIndexFromArray >= 1) {
                populateObjectsArray(reltioAPI);
            }
        } else {
            if (objects == null || fetchIndexFromArray >= objects.size()) {
                populateObjectsArray(reltioAPI);
            }
        }
    }

    protected void populateObjectsArray(ReltioAPIService reltioAPI) throws Exception {
        String requestBody = constructRequestBody();
        String requestURL = constructRequestURL();

        String responseJson = reltioAPI.post(requestURL, requestBody);
        populateResponseToObjectsArray(responseJson);
    }

    protected String constructRequestURL() {
        String requestURL;
        if (scanModeEnabled) {
            if (selectString == null) {
                requestURL = apiUrl + "/entities/_scan?filter=" + scanFilter + "&max=" + pageSize +"&select=uri"+"&activeness=active"+"&options=ovOnly"+"&scoreEnabled=false";
            } else {
                requestURL = apiUrl + "/entities/_scan?filter=" + scanFilter + "&max=" + pageSize + "&select=" + selectString;
            }
        } else {
            requestURL = apiUrl + "/entities/_dbscan";
        }
        return requestURL;
    }

    protected String constructRequestBody() {
        String requestBody = "";
            if (cursor != null && !cursor.isEmpty()) {
                requestBody = "{\"cursor\":{\"value\":\"" + cursor + "\"}}";
            }
        return requestBody;
    }

    protected void populateResponseToObjectsArray(String responseJson) throws IOException {
        Map<String, Object> obj = mapper.readValue(responseJson, Map.class);
        cursor = getCursorFromResponse(obj);
        if (obj.get("objects") != null) {
            objects = (List<Map<String, Object>>) obj.get("objects");
            uris = objects.stream()
                          .flatMap(m -> m.values()
                                         .stream()
                                         .map(x -> x.toString()))
                          .collect(Collectors.toList());
        } else {
            uris = null;
        }
    }

    protected String getCursorFromResponse(Map<String, Object> obj) {
        if (obj != null) {
            Map<String, Object> cursorObj = (Map<String, Object>) obj.get("cursor");
            if (cursorObj != null) {
                String cursor;
                if (!scanModeEnabled) {
                    cursor = (String) cursorObj.get("cursor");
                } else {
                    cursor = (String) cursorObj.get("value");
                }
                return cursor;
            }
        }
        return "";
    }
    
    public int getMaxPages() {
        return maxPages;
    }
    
    public void setMaxPages(int maxPages) {
        this.maxPages = maxPages;
    }
}
