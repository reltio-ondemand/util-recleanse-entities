package com.reltio.scripts.customer;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RecleanseEntities {
    
    private static Gson gson = new Gson();
    public static void main(String[] args) throws Exception {
    
        System.out.println("Process Started..");
        Properties properties = new Properties();
        ReltioScanService reltioScanService;
    
        try {
            String propertyFilePath = args[0];
            FileReader fileReader = new FileReader(propertyFilePath);
            properties.load(fileReader);
        } catch (Exception e) {
            System.out.println("Failed to Read the Properties File :: ");
            e.printStackTrace();
        }
    
        final String api = properties.getProperty("API_URL");
        final String entityType = properties.getProperty("ENTITY_TYPE");
        final String username = properties.getProperty("USERNAME");
        final String password = properties.getProperty("PASSWORD");
        final String output = properties.getProperty("OUTPUT");
        final String scanFilter = properties.getProperty("SCAN_FILTER");
        final String maxCount = properties.getProperty("MAX_COUNT");
        final String tasksApi = properties.getProperty("TASKS_API");
        final String maxPages = properties.getProperty("MAX_PAGES");
        

    
        final BufferedWriter b = new BufferedWriter(new FileWriter(output));
        
        
        final String SCAN_URL = "entities/_scan?select=uri&max="+maxCount+"&activeness=active&options=ovOnly&scoreEnabled=false&select=uri";
        final String scanUrl = api + SCAN_URL;
        String initialJSON = null;
    
        reltioScanService = new ReltioScanService();
        reltioScanService.setPageSize(Integer.parseInt(maxCount));
        reltioScanService.scan(scanFilter);
        reltioScanService.setApiUrl(api);
        reltioScanService.setReturnUriOnly(true);
        reltioScanService.setMaxPages(maxPages.isEmpty() ? Integer.MAX_VALUE : Integer.parseInt(maxPages));
        TokenGeneratorService tokenGeneratorService = null;
        try {
            tokenGeneratorService = new TokenGeneratorServiceImpl(username, password, "https://auth.reltio.com/oauth/token");
        } catch (APICallFailureException e) {
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
        } catch (GenericException e) {
            System.out.println(e.getExceptionMessage());
        }
        final ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
    
        final int[] count = {0};
        boolean eof = false;
        int threadsNumber = 100;
        ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber);
        ArrayList<Future<String>> futures = new ArrayList<>();
        int scans = 1;
        while (reltioScanService.hasNext(reltioAPIService)) {
                Thread.sleep(2000);
                System.out.println("Retrieving entities for cleanse...ScanCount: "+scans);
                List<String> uris = reltioScanService.getNext(reltioAPIService);
                System.out.println("Retrieved entities for cleanse...[ScanCount= "+scans+", entityCount = "+uris.size());
                    if (uris != null && uris.size() > 0) {
                        futures.add(executorService.submit(new Callable<String>() {
                            public String call() throws Exception {
                                String jsonList = new Gson().toJson(uris);
                                System.out.println("Writing job info to log...");
                                //starting recleanse task
                                String cleanseTaskApi = api+ "/cleanse?entityType="+entityType+"&distributed=true&taskPartsCount=8&forceCleansing=true";
                                String taskResponse = reltioAPIService.post(cleanseTaskApi, jsonList);
                                JsonArray taskJson = new Gson().fromJson(taskResponse, JsonArray.class);
                                String jobId = taskJson.get(0).getAsJsonObject().get("id").getAsString();
                                String status = taskJson.get(0).getAsJsonObject().get("status").getAsString();
                                System.out.println("Created cleanse job, jobId= "+jobId+ ", status= "+status);
                                String createdTime = taskJson.get(0).getAsJsonObject().get("createdTime").getAsString();
                                b.write("[Jobid : "+jobId+", createdTime: "+createdTime+ ", uriList = "+jsonList+"]");
                                b.newLine();
                                b.flush();
                                return jobId;
                            }
                        }));
                    } else {
                        break;
                    }
                    scans++;
                }
        waitForTasksComplete(futures, tasksApi, reltioAPIService);
        executorService.shutdown();
        b.close();
        System.out.println("Process Ended..");
    }
    
    public static void waitForTasksComplete(Collection<Future<String>> futures, String tasksApi, ReltioAPIService reltioAPIService) throws Exception {
        System.out.println("Checking  job statuses....");
        List<Future<String>> futureList = new ArrayList<>(futures);
        int completedCount = 0;
        while (futureList.size() > 0) {
            System.out.println("Jobs in pending status: "+futureList.size());
            System.out.println("Jobs Completed: "+completedCount);
            Thread.sleep(5000);
            Iterator<Future<String>> it = futureList.iterator();
            while (it.hasNext()) {
                Future<String> future = it.next();
                String jobId = future.get();
                String jobStatusApi = tasksApi+ "/"+jobId;
                String taskResponse = reltioAPIService.get(jobStatusApi);
                JsonObject taskJson = new Gson().fromJson(taskResponse, JsonObject.class);
                String status = taskJson.get("status").getAsString();
                System.out.println("[Job id:  "+jobId + ", status = "+status+"]");
                if (status.equals("COMPLETED") || status.equals("FAILED") || status.equals("CANCELED")) {
                    it.remove();
                    completedCount++;
                }
            }
        }
        System.out.println("All jobs completed..");
    }
    
}
