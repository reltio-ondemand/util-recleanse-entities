# Recleanse Entities satisfying filter condition#

# Description #

This script is used to recleanse entities based on given filter

# How to run script #

* Build project using mvn clean install -U and find jar in target folder or download jar (util-recleanse-entities.jar) from {{Project}}/jar/ location


* Usage
```
     nohup java -jar -Xmx2G -Xms2G util-recleanse-unmerge-entities.jar {{properties-file-path}} > {output-log-filename} 2>&1 &
```
* Properties file
```
   API_URL=https://361.reltio.com/reltio/api/RHxkaqNQUipxGDw
   TASKS_API = https://361.reltio.com/reltio/RHxkaqNQUipxGDw/tasks
   ENTITY_TYPE=person
   USERNAME=user@reltio.com
   PASSWORD=*******
   OUTPUT=cleanse_output_02_23.txt
   MAX_COUNT=2000 #count of entites cleansed in a single batch
   SCAN_FILTER=(equals(type,'configuration/entityTypes/person') and (inSameAttributeValue(missing(attributes.address.cleansedVerificationStatus)) and exists(attributes.address)))

```

* Example:
```
     nohup java -jar -Xmx2G -Xms2G utilutil-recleanse-unmerge-entities.jar recleanse.properties > cleanse_log_02_21_1.txt 2>&1 &
```
